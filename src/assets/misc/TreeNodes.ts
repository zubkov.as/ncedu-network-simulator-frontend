interface DataNode {
  name: string;
  children?: DataNode[];
}

interface FlatNode {
  expandable: boolean;
  name: string;
  level: number;
}
