import {Item} from './Item';
import {Device} from './Device';
import {Port} from './Port';

export class Link extends Item {
  idA: number;
  idZ: number;
  isUp: boolean;
  interface1: string;
  interface2: string;
  portA: Port;
  portZ: Port;

  x1: number;
  x2: number;
  y1: number;
  y2: number;

  public setADevice(device: Device) {
    this.x1 = device.x;
    this.y1 = device.y;
    this.interface1 = device.typeName;
    this.idA = device.id;
  }

  public setZDevice(device: Device) {
    this.x2 = device.x;
    this.y2 = device.y;
    this.interface2 = device.typeName;
    this.idZ = device.id;
  }


  connectedTo(activeDevice: Device) {
    return this.idA === activeDevice.id || this.idZ === activeDevice.id;
  }

  setAPort(port: Port) {
    this.portA = port;
  }
  setZPort(port: Port) {
    this.portZ = port;
  }
}
