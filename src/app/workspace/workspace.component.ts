import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Device} from '../Device';
import {Item} from '../Item';
import {ItemService} from '../item.service';
import {Link} from '../Link';
import {MatDialog} from '@angular/material';
import {ConsoleComponent} from '../console/console.component';
import {WorkbenchComponent} from '../workbench/workbench.component';
import {Port} from '../Port';
import {Subject} from 'rxjs';


@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {

  paletteOpened: boolean;

  private deviceA: Device;
  private deviceZ: Device;
  private ifLinking: boolean;
  private isActiveItem: boolean;

  private activeLink: Link;
  private deviceALinked: boolean;
  private portSelection: boolean;


  @ViewChild('workbench-box-router', {static: false}) element: ElementRef;
  private activeItem: Item;
  private deviceBeingAdded: Device;
  private serialId = 0;
  private devicesMap = new Map();
  private linksMap = new Map();
  private changeTreeEvent: Subject<DataNode[]> = new Subject<DataNode[]>();

  /* to be deleted */
  private portIdSerializer: number;
  private activePort: Port;

  constructor(
    public consoleDialog: MatDialog,
    private itemService: ItemService) {
  }

  ngOnInit() {

    this.portIdSerializer = 0;

    this.activeItem = null;
    this.isActiveItem = false;
    this.ifLinking = false;
    this.deviceALinked = false;
    this.deviceALinked = false;
    this.ifLinking = false;
    this.portSelection = false;
    this.deviceA = new Device();
    this.deviceZ = new Device();

    // this.itemService.getSwitches()
    //   .subscribe(switches => this.devices.push(...switches));
    // this.itemService.getRouters()
    //   .subscribe(routers => this.devices.push(...routers));
  }

  addNewItem(item: Item) {
    if (item.typeName === 'switch' ||
      item.typeName === 'router') {
      this.deviceBeingAdded = new Device();
      this.deviceBeingAdded.typeName = item.typeName;
      this.deviceBeingAdded.id = this.serialId++;
      this.deviceBeingAdded.x = 0;
      this.deviceBeingAdded.y = 0;
      this.deviceBeingAdded.name = item.typeName + '-' + this.deviceBeingAdded.id;

      console.log('created name: ' + this.deviceBeingAdded.name);
      // this.itemService.createDevice(item)
      //   .subscribe(device => this.workingDevices.push(device));
      this.deviceBeingAdded.ports = [
        new Port(this.portIdSerializer++, 'Fa1/0/1', 'mac' + this.serialId),
        new Port(this.portIdSerializer++, 'Fa1/0/2', 'mac' + this.serialId + 1),
        new Port(this.portIdSerializer++, 'Fa1/0/3', 'mac' + this.serialId + 2)
      ];
      this.devicesMap.set(this.deviceBeingAdded.id, this.deviceBeingAdded);
      this.deviceBeingAdded = null;
    }

    if (item.typeName === 'link' && this.ifLinking) {
      this.ifLinking = false;
    } else if (item.typeName === 'link'
      && this.devicesMap.size > 1) {
      this.ifLinking = true;
    }
    this.paletteOpened = false;
  }

  showItemMenu(item: Item) {
    if (item === this.activeItem) {
      this.deactivateItem(item);
    } else {
      this.activateItem(item);
      this.emitChangeTree();
    }
  }

  deleteActiveItem() {
    if (this.activeItem instanceof Device) {

      const linkKeys = this.linksMap.keys();
      for (const linkKey of linkKeys) {
        const link = this.linksMap.get(linkKey);
        if (link.connectedTo(this.activeItem as Device)) {
          this.linksMap.delete(link.id);
        }
      }
      this.devicesMap.delete((this.activeItem as Device).id);

      this.isActiveItem = false;
      this.activeItem = null;
    }
    if (this.activeItem instanceof Link) {
      this.linksMap.delete((this.activeItem as Link).id);
      this.isActiveItem = false;
    }
  }

  private linkDevice(clickedDevice: Device) {
    if (this.ifLinking) {
      if (this.deviceALinked) {
        if (this.activeLink.idA === clickedDevice.id) {
          return;
        }
        this.activeLink.setZDevice(clickedDevice);
        this.activeLink.id = this.serialId++;
        this.deviceZ = clickedDevice;
        this.portSelection = true;
      } else {
        this.activeLink = new Link();
        this.activeLink.setADevice(clickedDevice);
        this.deviceA = clickedDevice;
        this.deviceALinked = true;
      }
    }
  }

  updateLinks() {
    const linkKeys = this.linksMap.keys();
    for (const linkKey of linkKeys) {
      const link = this.linksMap.get(linkKey);
      link.x1 = this.devicesMap.get(link.idA).x + WorkbenchComponent.CENTER_OFFSET;
      link.y1 = this.devicesMap.get(link.idA).y + WorkbenchComponent.CENTER_OFFSET;
      link.x2 = this.devicesMap.get(link.idZ).x + WorkbenchComponent.CENTER_OFFSET;
      link.y2 = this.devicesMap.get(link.idZ).y + WorkbenchComponent.CENTER_OFFSET;
    }
  }

  emitChangeTree() {
    const newDataTree = [{
      name: 'Ports',
      children: this.asNodes((this.activeItem as Device).ports)
    }];
    this.changeTreeEvent.next(newDataTree);
  }

  clear() {
    this.linksMap.clear();
    this.devicesMap.clear();
  }

  stopLinking() {
    if (this.deviceALinked) {
      this.activeLink = null;
      this.deviceALinked = false;
    }
    this.ifLinking = false;
  }

  private selectPorts(ports: Map<number, Port>) {

    // this.itemService.createLink(this.activeLink)
    //   .subscribe(link => this.links.push(link));
    // console.log('link created: id ' + this.activeLink.id);
    const portKeys = ports.keys();
    this.activeLink.portA = ports.get(portKeys.next().value);
    this.activeLink.portZ = ports.get(portKeys.next().value);

    this.linksMap.set(this.activeLink.id, this.activeLink);

    this.setLinkedOn(this.activeLink.portA);
    this.setLinkedOn(this.activeLink.portZ);

    this.portSelection = false;
    this.deviceALinked = false;
    this.activeLink = null;
    this.updateLinks();
  }

  private setLinkedOn(port: Port) {
    const deviceKeys = Array.from(this.devicesMap.keys());
    for (const devkey of deviceKeys) {
      const device = this.devicesMap.get(devkey);
      const portIndex = device.ports.indexOf(port);
      if (portIndex >= 0) {
        device.ports[portIndex].linked = true;
        this.devicesMap.set(device.id, device);
      }
    }
  }

  private asNodes(ports: Port[]) {
    const portNodes = [];
    for (const port of ports) {
      portNodes.push({
        name: port.name,
        children: []
      });
    }
    return portNodes;
  }

  private deactivateItem(item: Item) {
    this.activeItem = null;
    this.isActiveItem = false;
    if (this.activeItem instanceof Device) {
      const newDevice = this.devicesMap.get((this.activeItem as Device).id);
      newDevice.isActive = false;
      this.devicesMap.set(newDevice.id, newDevice);
    } else {
      const newLink = this.linksMap.get((this.activeItem as Link).id);
      newLink.isActive = false;
      this.linksMap.set(newLink.id, newLink);
    }
    this.activeItem = null;
  }

  private activateItem(item: Item) {
    this.activeItem = item;
    this.isActiveItem = true;
    if (this.activeItem instanceof Device) {
      const newDevice = this.devicesMap.get((this.activeItem as Device).id);
      newDevice.isActive = true;
      this.devicesMap.set(newDevice.id, newDevice);
    } else {
      const newLink = this.linksMap.get((this.activeItem as Link).id);
      newLink.isActive = true;
      this.linksMap.set(newLink.id, newLink);
    }
  }

  openConsolePopup(): void {
    const dialogRef = this.consoleDialog.open(ConsoleComponent, {
      width: '800px',
      data: {name: 'andrew\'s', console: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  showPortConfig(port: Port) {
    this.activePort = port;
  }

  closeActivePort() {
    this.activePort = null;
  }

  setActivePortIP(ipString: string) {
    // this.activePort.ip = ipString;

    const updatedPort = (this.activeItem as Device).getPortByName(this.activePort.name);
    const indexToUpdate = (this.activeItem as Device).ports.indexOf(this.activePort);

    updatedPort.ip = ipString;
    (this.activeItem as Device).ports[indexToUpdate] = updatedPort;
    this.devicesMap.set(this.activeItem.id, this.activeItem as Device);
    console.log();
  }

  setActivePortMask(maskString: string) {
    // this.activePort.mask = maskString;
    (this.activeItem as Device).getPortByName(this.activePort.name).mask = maskString;
    this.devicesMap.set(this.activeItem.id, this.activeItem as Device);
  }
}
