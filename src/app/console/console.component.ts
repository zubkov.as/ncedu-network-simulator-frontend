import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

class consoleData {
  name: string;
  console: string;
}

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.css']
})
export class ConsoleComponent {

  constructor(
    public dialogRef: MatDialogRef<ConsoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: consoleData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
