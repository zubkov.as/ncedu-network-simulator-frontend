import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Link } from './Link';
import { Device } from './Device';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private switchUrl = '/switch';
  private routerUrl = '/router';
  private getUrl = '/get';
  private createUrl = '/add';
  private updateUrl = '/update';
  private simulationUrl = '/simulation';
  private linkUrl = '/link';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  constructor(
    private messageService: MessageService,
    private http: HttpClient
  ) { }

  getRouters() {
    return this.http.get<Device[]>(this.routerUrl + this.getUrl)
      .pipe(catchError(this.handleError<Device[]>('getRouters', [])));
  }

  getSwitches() {
    return this.http.get<Device[]>(this.switchUrl + this.getUrl)
      .pipe(catchError(this.handleError<Device[]>('getSwitches', [])));
  }

  getLinks() {
    return this.http.get<Link[]>(this.routerUrl + this.getUrl)
      .pipe(catchError(this.handleError<Link[]>('getLinks', [])));
  }

  createDevice(device: Device) {
    // http://hostname:port/router/add
    return this.http.post<Device>('/' + device.typeName + this.createUrl, device, this.httpOptions)
      .pipe(catchError(this.handleError<Device>('createDevice', device)));
  }

  createLink(link: Link) {
    return this.http.post<Link>(this.linkUrl + this.createUrl, link, this.httpOptions)
      .pipe(catchError(this.handleError<Link>('createLink', link)));
  }

  updateDevicePos(devId: number, x: number, y: number) {
  }

  /**
   * handles Http operation that failed
   * 1 reports error to console
   * 2 builds a user friendly message
   * 3 returns a safe value <T> so as to not bring the entire system down
   *
   * @param operation - string with the name of failed operation
   * @param result - alternative value to return as an Observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send error to some logging infrastructure
      console.error(error); // this is just a dummy

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  /**
   * uses messageService to log a message
   */
  private log(message: string) {
    this.messageService.add(`PlayerService: ${message}`);
  }

}
