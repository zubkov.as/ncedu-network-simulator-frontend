import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortSelectionComponent } from './port-selection.component';

describe('PortSelectionComponent', () => {
  let component: PortSelectionComponent;
  let fixture: ComponentFixture<PortSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
