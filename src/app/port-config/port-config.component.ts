import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Port} from '../Port';

@Component({
  selector: 'app-port-config',
  templateUrl: './port-config.component.html',
  styleUrls: ['./port-config.component.css']
})
export class PortConfigComponent implements OnInit {

  @Input() port: Port;
  @Output() closePortConfig = new EventEmitter();
  @Output() setIPEmitter = new EventEmitter<string>();
  @Output() setMaskEmitter = new EventEmitter<string>();
  private ipDisabled: boolean;
  private maskDisabled: boolean;

  private ipPattern = '([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}';
  private maskPattern = '(3[0-2]|[1-2]\\d|\\d)$';


  constructor() {
    this.ipDisabled = true;
    this.maskDisabled = true;
  }

  ngOnInit() {
  }

  closeConfig() {
    this.closePortConfig.emit();
  }

  submitIP() {
    this.ipDisabled = true;
    this.setIPEmitter.emit(this.port.ip);
  }

  submitMask() {
      this.maskDisabled = true;
      this.setMaskEmitter.emit(this.port.mask);
  }

  activateIP() {
    this.ipDisabled = false;
  }
  activateMask() {
    this.maskDisabled = false;
  }

  isIPActive() {
    return !this.ipDisabled;
  }

  isMaskActive() {
    return !this.maskDisabled;
  }
}
