import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {CdkDragEnd} from '@angular/cdk/drag-drop';

import {Device} from '../Device';
import {Link} from '../Link';
import {ItemService} from '../item.service';
import {Item} from '../Item';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-workbench',
  templateUrl: './workbench.component.html',
  styleUrls: ['./workbench.component.css']
})
export class WorkbenchComponent implements OnInit {

  public static CENTER_OFFSET = 40;
  @Input() linksMap = new Map();
  @Input() devicesMap = new Map();
  @Input() desactivateItemEvent: Observable<any>;
  @Output() showDeviceMenu = new EventEmitter<Item>();
  @Output() linkDeviceEmission = new EventEmitter<Device>();


  routerImagePath: string;
  switchImagePath: string;

  private deviceALinked: boolean;
  private innerWidth: number;
  private innerHeight: number;

  constructor(
    private itemService: ItemService,
  ) {
    this.routerImagePath = 'images/router.png';
    this.switchImagePath = 'images/switch.png';
  }

  ngOnInit() {
    this.deviceALinked = false;
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;

    // this.itemService.getSwitches()
    //   .subscribe(switches => this.devices.push(...switches));
    // this.itemService.getRouters()
    //   .subscribe(routers => this.devices.push(...routers));
    // this.itemService.getLinks()
    //   .subscribe(links => this.links.push(...links));
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
  }

  updatePosition($event: CdkDragEnd<any>, id: number) {
    this.devicesMap.get(id).x = $event.source.getFreeDragPosition().x;
    this.devicesMap.get(id).y = $event.source.getFreeDragPosition().y;

    this.itemService.updateDevicePos(id,  this.devicesMap.get(id).x,
                                          this.devicesMap.get(id).y);

    this.updateLinks();
  }

  updateLinks() {
    const linkKeys = this.linksMap.keys();
    for (const linkKey of linkKeys) {
      const link = this.linksMap.get(linkKey);
      link.x1 = this.devicesMap.get(link.idA).x + WorkbenchComponent.CENTER_OFFSET;
      link.y1 = this.devicesMap.get(link.idA).y + WorkbenchComponent.CENTER_OFFSET;
      link.x2 = this.devicesMap.get(link.idZ).x + WorkbenchComponent.CENTER_OFFSET;
      link.y2 = this.devicesMap.get(link.idZ).y + WorkbenchComponent.CENTER_OFFSET;
    }
  }

  getKeys(map) {
    return Array.from(map.keys());
  }

  onClick(clickedDevice: Device) {
    this.linkDeviceEmission.emit(clickedDevice);
  }

  onRightClick(clickedItem: Item): boolean {
    this.showDeviceMenu.emit(clickedItem);
    return false;
  }

  isRouter(device: Device) {
    return device.typeName === 'router';
  }

  isSwitch(device: Device) {
    return device.typeName === 'switch';
  }

  isSwitchActive(device: Device) {
    return device.typeName === 'switch' && device.isActive;
  }

  isRouterActive(device: Device) {
    return device.typeName === 'router' && device.isActive;
  }
}
