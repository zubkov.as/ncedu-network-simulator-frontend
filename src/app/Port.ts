export class Port {
  id: number;
  name: string;
  macAddress: string;
  linked: boolean;
  shutdown: boolean;
  ip: string;
  mask: string;

  constructor(id: number, name: string, mac: string) {
    this.id = id;
    this.name = name;
    this.macAddress = mac;
    this.linked = false;
    this.shutdown = true;
    this.ip = '';
    this.mask = '';
  }

  public getInfoAsNodes() {
    return [
      // {name: 'id: ' + this.id, children: []},
      // {name: 'name: ' + this.name, children: []},
      // {name: 'MAC: ' + this.macAddress, children: []},
      // {name: 'linked: ' + this.linked, children: []},
      // {name: 'shutdown: ' + this.shutdown, children: []},
      // {name: 'ip address: ' + this.ip, children: []},
      // {name: 'mask: ' + this.mask, children: []}
    ];
  }
}
