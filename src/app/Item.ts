export class Item {
  id: number;
  isActive: boolean;
  typeName: string;
}
