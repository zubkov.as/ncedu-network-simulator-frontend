import {Item} from './Item';
import {Port} from './Port';

export class Device extends Item {
  name: string;
  y: number;
  x: number;
  ports: Port[];

  getPortByName(name: string) {
    for (const port of this.ports) {
      if (port.name === name) {
        return port;
      }
    }
  }
}
